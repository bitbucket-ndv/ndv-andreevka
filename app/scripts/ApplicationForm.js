import {transitionShow, transitionHide} from './utils';

export default {
	init: function() {
		this.container = document.querySelector('.application');
		this.application = document.querySelector('.app-form');
		this.isOpen = false;

		// preload section background image after page load
		window.addEventListener('load', this.preload.bind(this), false);
		$('.application-form, .feedback').on('click', (event) => {
			event.preventDefault();
			this.toggleState();
		});
		$('.close', this.container).click(this.toggleState.bind(this));
		$('input, textarea', this.container)
			.each(function() {
				if ($(this).val().trim() === '') {
					$(this).addClass(' is-empty');
				}
			})
			.on('blur', event => {
				const $elm = $(event.target);
				$elm.removeClass('is-empty');
				if ($elm.val() === '') {
					$elm.addClass(' is-empty');
				}
			});
	},

	toggleState: function() {
		this.isOpen = !this.isOpen;
		if (this.isOpen) {
			transitionShow(this.container, 'is-open');
		} else {
			transitionHide(this.container, 'is-open');
		}
	},

	preload: function() {
		const imagePath = '/pic/design';
		const images = ['application-back.jpg'];
		images.forEach(filename => {
			let img = new Image();
			img.src = `${imagePath}/${filename}`;
		});
	}
}
