import {transitionShow, transitionHide} from '../utils';
import '../libs/parallax';
import loadJS from '../utils/loadJS';

let map;

const opts = {
	'index': {
		autoplay: false,
		dots: false,
		prevArrow: '<span class="prev"/>',
		nextArrow: '<span class="next"/>',
		draggable: false,
		slide: '.slider-advantage__item',
	},
	'image': {
		arrows: false,
		autoplay: false,
		dots: true,
		draggable: false,
	},
	'gallery': {
		autoplay: false,
		dots: false,
		prevArrow: '<span class="prev"/>',
		nextArrow: '<span class="next"/>',
		draggable: false,
		slide: '.slider-gallery__item',
	},
}

const page = {
	initialize: function() {
		page.bindHandlers();
		page.initSliders();
		const leafParallax = new Parallax(document.querySelector('.parallax-container .layers'));
		const bubbleParallax = new Parallax(document.querySelector('.about-bubbles'));
		loadJS('https://api-maps.yandex.ru/2.1/?lang=ru_RU')
			.then(page.onMapLoad);
	},

	bindHandlers: function() {
		$('.about-all-link, .slider-advantage .close').on('click', page.toggleAdvantageSection);

		$('.gallery-bubble, .slider-gallery .close').on('click', page.toggleGallery);
	},

	onMapLoad: function() {
		ymaps.ready(function() {
			map = new ymaps.Map('map', {
				center: [55.980907, 37.114695],
				zoom: 14,
				controls: [],
			});
			require.ensure('../map/infrastructure', () => {
				const points = require('../map/infrastructure');
				points.forEach(data => page.setIcon(data));
			}, 'infrastructure');

			map.geoObjects.add(new ymaps.Placemark([55.9792, 37.125089], {}, {
				iconLayout: 'default#image',
				iconImageHref: '/pic/design/tooltip-andreevka.png',
				iconImageSize: [72, 85],
				iconImageOffset: [-36, -85]
			}));
		});
	},

	setIcon: function(data) {
		const marker = new ymaps.Placemark(data.coords, {}, {
			iconLayout: 'default#image',
			iconImageHref: data.icon.src,
			iconImageSize: [data.icon.w, data.icon.w],
			iconImageOffset: [-data.icon.w/2, -data.icon.h]
		});
		map.geoObjects.add(marker);
	},

	initSliders: function() {
		$('.text-slider')
			.slick({dots: true, arrows: false, autoplay: true, draggable: false})
			.on('afterChange', function(event, slick, currentSlide) {
				// $(this).attr('data-slide', currentSlide);
			});
		$('.text-slider__item').on('click', function() {
			const index = $(this).attr('data-slide');
			page.toggleAdvantageSection(index);
		});
	},

	toggleGallery: function() {
		const $slider = $('.slider-gallery');

		if ($slider.hasClass('is-open')) {
			transitionHide($slider[0], 'is-open').then(() => $slider.slick('unslick'));
		} else {
			transitionShow($slider[0], 'is-open').then(() => $slider.slick(opts.gallery));
		}
	},

	toggleAdvantageSection: function(index) {
		const $slider = $('.slider-advantage');

		if ($slider.hasClass('is-open')) {
			transitionHide($slider[0], 'is-open').then(() => {
				$slider.slick('unslick');
				$slider.find('.image-slider').slick('unslick');
			});
		} else {
			transitionShow($slider[0], 'is-open').then(() => {
				$slider.slick(opts.index).slick('slickGoTo', index || 1);
				$slider.find('.image-slider').slick(opts.image);
			});
		}
	}
};

export default page;
