
export function getInfoContent(properties) {
	let content = `<div class="balloon-layout">
		<div class="balloon-layout__header" style="background-image: url(${properties.image})"></div>
		<h3 class="balloon-layout__title">${properties.title}</h3>
		<ul class="balloon-layout__props">`;

	if (properties.address) {
		content += `<li class="balloon-layout__prop address">${properties.address}</li>`;
	}
	if (properties.metro) {
		content += `<li class="balloon-layout__prop metro">${properties.metro}</li>`;
	}
	if (properties.tel) {
		content += `<li class="balloon-layout__prop tel">${properties.tel}</li>`;
	}
	if (properties.worktime) {
		content += `<li class="balloon-layout__prop worktime">${properties.worktime}</li>`;
	}

	content += `</ul></div>`;

	return content;
}
