import scrollbar from 'perfect-scrollbar';
import pages from './pages';

export default function() {
	const route = document.querySelector('.page').getAttribute('data-type');

	switch (route) {
		case 'index':
			pages.index.initialize();
			break;
		case 'about':
			pages.about.initialize();
			break;
		case 'document':
			pages.document.initialize();
			break;
		case 'location':
			pages.location.initialize();
			break;
		case 'contacts':
			pages.contact.initialize();
			break;
		case 'mortage':
			pages.mortage.initialize();
			break;
		case 'flat':
			pages.flat.initialize();
			break;
		case 'construction-detail':
			$('[rel="fancybox"]').fancybox();
			break;
		case 'participants':
			$('.participant .button-plus').on('click', function() {
				$(this).toggleClass('is-active');
				$(this).parents('.participant').toggleClass('is-open');
				scrollbar.update(document.querySelector('.scrollbar'));
			});
			break;
	}
}
