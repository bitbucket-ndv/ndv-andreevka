export default function() {
	const videoElm = document.getElementById('video');

	if (!videoElm) {
		return;
	}

	const videoPlayer = videojs('video');

	$('.video-button, .action-video').fancybox({
		scrolling: 'no',
		href: '#video',
		fitToView: true,
		width: '100%',
		height: '100%',
		padding: 0,
		margin: 0,
		autoSize: false,
		afterShow: () => videoPlayer.play(),
		afterClose: () => videoPlayer.pause()
	});

}
