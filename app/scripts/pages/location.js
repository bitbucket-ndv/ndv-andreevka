import assign from 'object-assign';
import scrollbar from 'perfect-scrollbar';
import '../libs/fancybox';
import loadJS from '../utils/loadJS';
import layouts from '../map/balloonTemplates';
import {getMarkerOpts} from '../map/googleMarker';
import {getInfoContent} from '../map/googleTemplate';

let map;

window.locationMapInit = function() {
	map = new google.maps.Map(document.getElementById('location-map'), {
		center: {lat: 55.97881, lng: 37.130089},
		zoom: 15,
		mapTypeControl: false,
		mapTypeId: google.maps.MapTypeId.HYBRID,
		zoomControl: false
	});
	map.panBy(200, 0);
	page.onMapLoad();
}

const page = {
	initialize: function() {
		page.routes = [];
		page.bindHandlers();
		loadJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyCwsCTj2EoKgZv80xYhVIU6vD5508kydqY&callback=locationMapInit');
	},

	onMapLoad: function() {
		const coords = [
			[{lat: 55.97192371255816, lng: 37.096584570744696}, {lat: 55.97334358460237, lng: 37.1008331898243}, {lat: 55.974979982383125, lng: 37.10658384595224}, {lat: 55.97550939031174, lng: 37.10877252850838}, {lat: 55.97507623891133, lng: 37.11280657086677}, {lat: 55.974931854028824, lng: 37.11503816876716}, {lat: 55.97601472745621, lng: 37.11885763440437}, {lat: 55.97624333017702, lng: 37.11902929578134}, {lat: 55.97713366468405, lng: 37.118836176732266}, {lat: 55.97802397861141, lng: 37.11881471906013}, {lat: 55.97872177784415, lng: 37.11898638043708}, {lat: 55.97951580986894, lng: 37.11924387250252}, {lat: 55.980382007953416, lng: 37.11926533017463}, {lat: 55.98068276662065, lng: 37.11945844922371}, {lat: 55.98083916019939, lng: 37.119930518010335}, {lat: 55.980879256575335, lng: 37.12479266668701}, {lat: 55.981180, lng: 37.132712}, {lat: 55.978696, lng: 37.132691}, {lat: 55.978648, lng: 37.131232}],
			[{lat: 56.01266205282043, lng: 37.20521602178954}, {lat: 55.98428366332558, lng: 37.17950973059073}, {lat: 55.98240706633527, lng: 37.176848979247964}, {lat: 55.98077098407511, lng: 37.17393073583972}, {lat: 55.97961606062462, lng: 37.17135581518549}, {lat: 55.978148295440626, lng: 37.16830882574452}, {lat: 55.97735423522285, lng: 37.16646346594234}, {lat: 55.9771617333107, lng: 37.16551932836901}, {lat: 55.97706548199364, lng: 37.16260108496074}, {lat: 55.977113607682135, lng: 37.149211497558355}, {lat: 55.98363408241079, lng: 37.146808238281004}, {lat: 55.982233, lng: 37.138055}, {lat: 55.981584, lng: 37.136274}, {lat: 55.981127, lng: 37.132798}, {lat: 55.978696, lng: 37.132691}, {lat: 55.978648, lng: 37.131232}]
		];
		coords.forEach(points => {
			const route = new google.maps.Polyline({
				strokeColor: '#93bd46',
				strokeOpacity: 1.0,
				strokeWeight: 5,
				path: points
			});
			route.setMap(map);
			page.routes.push(route);
		});
		page.setMapMarkers();
	},

	bindHandlers: function() {
		$('.location .close').on('click', function() {
			$(this).parents('.location').toggleClass('is-open');
		});

		$('.transport-type__item').on('click', function() {
			const type = $(this).attr('data-type');
			$(this).addClass('is-active').siblings().removeClass('is-active');
			$('.location-method__item')
					.removeClass('is-open')
					.filter('.location-method__item_' + type)
					.addClass('is-open');
		});

		$('.genplan-bubble').fancybox();

		$('.route .route__item').on('click', function() {
			$(this).addClass('is-active').siblings().removeClass('is-active');
			page.setActiveRoute(parseInt($(this).attr('data-route'), 10));
		});
	},

	setActiveRoute: function(routeNum) {
		page.routes.forEach((r, i) => r.setOptions({visible: routeNum === i}));
	},

	setMapMarkers: function() {
		require.ensure('../map/points', () => {
			const points = require('../map/points');
			points.objects.forEach(data => page.setMarker('object', data));
		}, 'points');

		require.ensure('../map/infrastructure', () => {
			const points = require('../map/infrastructure');
			points.forEach(data => page.setIcon(data));
		}, 'infrastructure');
	},

	setMarker(type, data) {
		const marker = new google.maps.Marker(
			assign(
				{},
				{
					position: {lat: data.coords[0], lng: data.coords[1]},
					title: data.properties.title || '',
					map: map
				},
				getMarkerOpts(type)
			)
		);
		const infoWindow = new google.maps.InfoWindow({
			content: getInfoContent(data.properties)
		});

		marker.addListener('click', () => {
			infoWindow.open(map, marker);
			infoWindow.addListener('domready', function() {
				const iwOuter = $('.gm-style-iw');
				const iwBackground = iwOuter.prev();

				iwBackground.children(':nth-child(2)').css({'display' : 'none'});
				iwBackground.children(':nth-child(3)').addClass('balloon-tip');
				iwBackground.children(':nth-child(4)').css({'display' : 'none'});
			});
		});
	},

	setIcon(data) {
		const title = data.properties && data.properties.title || '';
		const marker = new google.maps.Marker(
			{
				position: {lat: data.coords[0], lng: data.coords[1]},
				title: title,
				map: map,
				icon: {
					url: data.icon.src,
					size: new google.maps.Size(data.icon.w, data.icon.h),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(0, data.icon.h)
				}
			}
		);
	}
};

export default page;
