
export function getMarkerOpts(type) {
	const objectMarkerOpts = {
		anchorPoint: new google.maps.Point(45, 23),
		icon: {
			url: '/pic/design/tooltip-andreevka.png',
			size: new google.maps.Size(72, 85),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(0, 85)
		},
		shape: {
			coords: [1, 1, 72, 85],
			type: 'rect'
		}
	};

	const officeMarkerOpts = {
		anchorPoint: new google.maps.Point(45, 23),
		icon: {
			url: '/pic/design/tooltip-ndv.png',
			size: new google.maps.Size(72, 85),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(0, 85),
		},
		shape: {
			coords: [1, 1, 72, 85],
			type: 'rect'
		}
	};

	switch(type) {
		case 'object':
			return objectMarkerOpts;
		case 'office':
			return officeMarkerOpts;
	}
}
