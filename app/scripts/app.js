import scrollbar from 'perfect-scrollbar';
import backnavWidget from './BackNav';
import applicationForm from './ApplicationForm';
import Parallax from './Parallax';
import actions from './actions';
import routes from './routes';
import './libs/fancybox';

actions();

routes();

applicationForm.init();

$('.scrollbar').each(function() {
	scrollbar.initialize(this, {suppressScrollX: true, maxScrollbarLength: 20, minScrollbarLength: 20});
});

let parallax = new Parallax({element: '.parallax-element'});

$('.footer .news__list').slick({
	dots: false,
	draggable: false,
	autoplay: true,
	slide: '.news__item',
	arrows: false,
	adaptiveHeight: true
});
