import map from '../map/contact';
import '../libs/fancybox';

const page = {
	initialize: function() {
		map.initialize();
		page.bindHandlers();
	},

	bindHandlers: function() {
		$('.contact .close').on('click', function() {
			$(this).parents('.contact').toggleClass('is-open');
		});

		$('.office-bubble').fancybox();

		$('.metro-selector').on('click', function() {
			$(this).toggleClass('is-open');
		});
		$('.metro-selector').on('click', '.selector__item', function() {
			const label = $(this).text();
			const container = $(this).parents('.metro-selector');
			container.find('.selector__label').text(label);
			map.activateOffice($(this).data('index'));
		});
		require.ensure('../map/points', () => {
			const points = require('../map/points');
			const dropdownElm = $('.metro-selector .selector__dropdown');
			points.offices.forEach((office, i) => {
				if (!office.properties.metro) {
					return;
				}
				dropdownElm
						.append($('<div class="selector__item">' + office.properties.metro + '</div>').data('index', i));
			});
		}, 'points');
	}
};

export default page;
