const infrastructure = [
	{coords: [55.9782, 37.126089], icon: {src: '/pic/design/map-railway.png', w: 34, h: 34}},
	{coords: [55.9782, 37.129489], properties: {title: 'Some title'}, icon: {src: '/pic/design/map-abc.png', w: 34, h: 34}}
];

export default infrastructure;
