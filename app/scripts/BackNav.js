
class Widget {
	constructor() {
		this.isActive = false;
		['show', 'close', 'onTransitionEnd', 'toggleState'].forEach(name => {
			this[name] = this[name].bind(this);
		});
		this.addHandlers();
	}

	addHandlers() {
		const button = document.getElementById('back-nav-switch');
		button.addEventListener('click', this.toggleState, false);

		this.perspectiveWrapper = document.body;
	}

	onTransitionEnd(event) {
		if ($(event.target).hasClass('page-viewport')) {
			this.perspectiveWrapper.className = this.perspectiveWrapper.className.replace(/\s+is-modalview/, '');
			$('.page-viewport').off('transitionend', this.onTransitionEnd);
			this.isActive = false;
		}
	}

	toggleState() {
		if (this.isActive) {
			this.close();
		} else {
			this.show();
		}
	}

	show(event) {
		this.perspectiveWrapper.className += ' is-modalview';
		setTimeout(() => {
			this.perspectiveWrapper.className += ' is-animate';
		}, 25);
		this.isActive = true;
	}

	close() {
		$('.page-viewport').on('transitionend', this.onTransitionEnd);
		this.perspectiveWrapper.className = this.perspectiveWrapper.className.replace(/\s+is-animate/, '');
	}

};

export default new Widget();
