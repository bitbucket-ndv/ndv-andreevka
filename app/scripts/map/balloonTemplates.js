const balloonTemplate = `
	<div class="balloon-layout">
			{% if properties.image %}
				<div class="balloon-layout__header" style="background-image: url({{ properties.image }})"></div>
			{% endif %}
			{% if properties.title %}<h3 class="balloon-layout__title">{{ properties.title }}</h3>{% endif %}
			<ul class="balloon-layout__props">
				{% if properties.address %}<li class="balloon-layout__prop address">{{ properties.address }}</li>{% endif %}
				{% if properties.metro %}<li class="balloon-layout__prop metro">{{ properties.metro }}</li>{% endif %}
				{% if properties.tel %}<li class="balloon-layout__prop tel">{{ properties.tel }}</li>{% endif %}
				{% if properties.worktime %}<li class="balloon-layout__prop worktime">{{ properties.worktime|raw }}</li>{% endif %}
			</li>
			<span class="close"></span>
	</div>`;

const balloonMethods = {
	build: function build() {
		this.constructor.superclass.build.call(this);

		this._$element = $('.balloon-layout', this.getParentElement());
		this.applyElementOffset();
		this._$element
			.find('.close')
			.on('click', $.proxy(this.onCloseClick, this));
	},

	applyElementOffset: function applyElementOffset() {
		const arrowHeight = 15;
		this._$element.css({
			left: -(this._$element[0].offsetWidth / 2),
			top: -(this._$element[0].offsetHeight + arrowHeight),
		});
	},

	onCloseClick: function onCloseClick(e) {
		e.preventDefault();

		this.events.fire('userclose');
	},
};

export default {
	create: function create() {
		const BalloonContentLayoutClass = ymaps.templateLayoutFactory.createClass(balloonTemplate, balloonMethods);
		ymaps.layout.storage.add('ndv#baloonLayout', BalloonContentLayoutClass);
	},

	balloonOptions: {
		balloonShadow: false,
		balloonLayout: 'ndv#baloonLayout',
		balloonPanelMaxMapArea: 0,
	},

	officeIcon: {
		iconLayout: 'default#image',
		iconImageHref: '/pic/design/tooltip-ndv.png',
		iconImageSize: [72, 85],
		iconImageOffset: [-36, -85],
	},
};
