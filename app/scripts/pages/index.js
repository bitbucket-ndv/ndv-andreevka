import aboutPage from './about';
import documentPage from './documents';
import locationPage from './location';
import contactPage from './contacts';
import mortagePage from './mortage';
import flatPage from './flat';

const index = {
	initialize: function() {
		$('.slider')
			.slick({
				arrows: false,
				autoplay: true,
				dots: true,
				speed: 800,
				autoplaySpeed: 7000
			})
			.on('afterChange', (event, slick, currentSlide) => {
				$('.frame-slider').slick('slickGoTo', currentSlide);
			});

		$('.frame-slider')
			.slick({
				arrows: false,
				autoplay: false,
				dots: false,
				fade: true
			});

		const $video = $('.viewport__slides video');
		if ($video.size() > 0) {
			$video.get(0).play();
		}
	}
};

export default {
	index,
	about: aboutPage,
	document: documentPage,
	location: locationPage,
	contact: contactPage,
	mortage: mortagePage,
	flat: flatPage
};
