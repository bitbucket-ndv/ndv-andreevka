import '../libs/fancybox';

const page = {
	initialize: function() {
		page.bindHandlers();
	},

	bindHandlers: function() {
		$('.ico-full').fancybox();
		$('.genplan-button').fancybox();
		$('.flat .close').on('click', () => history.back());
	}
};

export default page;
