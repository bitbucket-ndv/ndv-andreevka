import scrollbar from 'perfect-scrollbar';

export default {
	initialize: function() {
		scrollbar.initialize(document.querySelector('.document-list'), {suppressScrollX: true, maxScrollbarLength: 20});
	}
};