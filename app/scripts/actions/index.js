import camera from './camera';
import video from './video';
import tour from './tour';

export default function() {
	camera();
	video();
	tour();
}
