import assign from 'object-assign';
import loadJS from '../utils/loadJS';
import layouts from './balloonTemplates';
import {getMarkerOpts} from './googleMarker';
import {getInfoContent} from './googleTemplate';

let map;

window.contactMapInit = function() {
	map = new google.maps.Map(document.getElementById('contact-map'), {
		center: {lat: 55.97881, lng: 37.130089},
		zoom: 16,
		mapTypeControl: false,
		mapTypeId: google.maps.MapTypeId.HYBRID,
		zoomControl: false
	});
	module.setMapMarkers();
}

const module = {
	initialize: function() {
		module.officePoints = [];
		loadJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyCwsCTj2EoKgZv80xYhVIU6vD5508kydqY&callback=contactMapInit');
	},

	setMapMarkers: function() {
		require.ensure('./points', () => {
			const points = require('./points');
			points.objects.forEach(data => module.setMarker('object', data));
			points.offices.forEach(data => module.setMarker('office', data));
		}, 'points');
	},

	activateOffice: function(index) {
		const {infoWindow, marker} = module.officePoints[index];
		infoWindow.open(map, marker);
		map.setCenter(marker.getPosition());
		map.panBy(200, -100);
	},

	setMarker(type, data) {
		const marker = new google.maps.Marker(
			assign(
				{},
				{
					position: {lat: data.coords[0], lng: data.coords[1]},
					title: data.properties.title || '',
					map: map
				},
				getMarkerOpts(type)
			)
		);
		const infoWindow = new google.maps.InfoWindow({
			content: getInfoContent(data.properties)
		});
		infoWindow.addListener('domready', function() {
			const iwOuter = $('.gm-style-iw');
			const iwBackground = iwOuter.prev();

			iwBackground.children(':nth-child(1)').css({'display' : 'none'});
			iwBackground.children(':nth-child(2)').css({'display' : 'none'});
			iwBackground.children(':nth-child(3)').addClass('balloon-tip');
			iwBackground.children(':nth-child(4)').css({'display' : 'none'});
		});
		if (type === 'office') {
			module.officePoints.push({infoWindow, marker});
		}

		marker.addListener('click', () => {
			infoWindow.open(map, marker);
		});
	}
}

export default module;
