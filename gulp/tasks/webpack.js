import gulp from'gulp';
import webpack from 'webpack-stream';
import plumber from 'gulp-plumber';
import errorHandler from '../errorHandler';
import webpack_config from '../../webpack.config';
import config from '../config';

gulp.task('webpack', () => {
	return gulp
		.src('app/scripts/app.js')
		.pipe(plumber({errorHandler}))
		.pipe(webpack(webpack_config))
		.pipe(gulp.dest(config.scripts))
});
