const points = {
	objects: [{
		coords: [55.9792, 37.125089],
		properties: {
			title: 'Поселок Андреевка',
			address: 'Трубная пл., д.2',
			tel: '+7 (495) 988-44-22',
			image: 'http://www.ndv.ru/pic/office/71-mprev.jpg'
		}
	}],

	/*
Офисы НДВ-недвижимость
 */
	offices: [{
		properties: {
			title: 'Офис Андреевка',
			address: 'Трубная пл., д.2',
			metro: 'Трубная (Центральный офис)',
			tel: '+7 (495) 988-44-22',
			worktime: '9:00 - 20:00 / ежедневно',
			image: 'http://www.ndv.ru/pic/office/71-mprev.jpg'
		},
		coords: [55.97881, 37.124]
	}, {
		properties: {
			title: 'Центральный офис',
			address: 'Трубная пл., д.2',
			metro: 'Трубная (Центральный офис)',
			tel: '+7 (495) 988-44-22',
			worktime: '9:00 - 20:00 / ежедневно',
			image: 'http://www.ndv.ru/pic/office/71-mprev.jpg'
		},
		coords: [55.766491, 37.622333]
	}, {
		properties: {
			title: 'Отделение на Кутузовском',
			address: 'Кутузовский пр-т, д.18',
			metro: 'Киевская',
			tel: '+7 (495) 984-33-01',
			worktime: '9:00 - 20:00 / пн.-пт.',
			image: 'http://www.ndv.ru/pic/office/577-mprev.jpg'
		},
		coords: [55.746785, 37.552184]
	}, {
		properties: {
			title: 'Отделение Нагатинское',
			address: 'Варшавское шоссе, д.59',
			metro: 'Нагатинская',
			tel: '+7 (495) 984-33-03',
			worktime: '09:00 - 20:00 / пн.-пт.;<br>10:00 - 18:00 / сб.-вс.',
			image: 'http://www.ndv.ru/pic/office/607-mprev.jpg'
		},
		coords: [55.675641, 37.626951]
	}, {
		properties: {
			title: 'Отделение на Красной Пресне',
			address: 'ул. Красная Пресня, д. 29',
			metro: 'Улица 1905 года',
			tel: '+7 (495) 984-33-05',
			worktime: '09:00 - 20:00 / пн.-пт.;<br>10:00 - 18:00 / сб.-вс.',
			image: 'http://www.ndv.ru/pic/office/776-mprev.jpg'
		},
		coords: [55.762252, 37.564069]
	}, {
		properties: {
			title: 'Отделение на Войковской',
			address: 'Ленинградское ш., д. 19',
			metro: 'Войковская',
			tel: '+7 (495) 984-33-06',
			worktime: '09:00 - 20:00 / пн.-пт.;<br>10:00 - 18:00 / сб.-вс.',
			image: 'http://www.ndv.ru/pic/office/5523-mprev.jpg'
		},
		coords: [55.821805, 37.49444]
	}, {
		properties: {
			title: 'Юго-Западное отделение',
			address: 'ул. Профсоюзная, д.7/12',
			metro: 'Академическая',
			tel: '+7 (495) 988-44-23',
			worktime: '09:00 - 20:00 / пн.-пт.;<br>10:00 - 18:00 / сб.-вс.',
			image: 'http://www.ndv.ru/pic/office/308-mprev.jpg'
		},
		coords: [55.683097, 37.570384]
	}, {
		properties: {
			title: 'Северо-Западное отделение',
			address: 'ул. Марш. Бирюзова, д. 28',
			metro: 'Октябрьское',
			tel: '+7 (495) 988-44-24',
			worktime: '09:00 - 20:00 / пн.-пт.;<br>10:00 - 18:00 / сб.-вс.',
			image: 'http://www.ndv.ru/pic/office/80-mprev.jpg'
		},
		coords: [55.797387, 37.486661]
	}, {
		properties: {
			title: 'Юго-Восточное отделение',
			address: 'Братиславская, д. 13, к.1',
			metro: 'Братиславская',
			tel: '+7 (495) 988-44-26',
			worktime: '09:00 - 20:00 / пн.-пт.;<br>10:00 - 18:00 / сб.-вс.',
			image: 'http://www.ndv.ru/pic/office/79-mprev.jpg'
		},
		coords: [55.660297, 37.76027]
	}, {
		properties: {
			title: 'Северо-восточное отделение',
			address: 'Проспект Мира, д. 182',
			metro: 'ВДНХ',
			tel: '+7 (495) 988-44-28',
			worktime: '09:00 - 20:00 / пн.-пт.;<br>10:00 - 18:00 / сб.-вс.',
			image: 'http://www.ndv.ru/pic/office/420-mprev.jpg'
		},
		coords: [55.826104, 37.647846]
	}, {
		properties: {
			title: 'Отделение на Павелецкой',
			address: 'Б. Строченовский переулок, д. 4, стр.1',
			metro: 'Павелецкая',
			tel: '+7 (495) 988-44-29',
			worktime: '09:00 - 20:00 / пн.-пт.;<br>10:00 - 18:00 / сб.-вс.',
			image: 'http://www.ndv.ru/pic/office/416-mprev.jpg'
		},
		coords: [55.729807, 37.62978]
	}, {
		properties: {
			title: 'Отделение Савёловское',
			address: 'ул. Нижн. Масловка, д. 14',
			metro: 'Савёловская',
			tel: '+7 (495) 984-33-07',
			worktime: '09:00 - 20:00 / пн.-пт.;<br>10:00 - 18:00 / сб.-вс.',
			image: 'http://www.ndv.ru/pic/office/843-mprev.jpg'
		},
		coords: [55.792073, 37.575953]
	}, {
		properties: {
			title: 'Отделение на Красносельской',
			address: 'ул. Краснопрудная, 26',
			metro: 'Красносельская',
			tel: '+7 (495) 988-44-25',
			worktime: '09:00 - 20:00 / пн.-пт.;<br>10:00 - 18:00 / сб.-вс.',
			image: 'http://www.ndv.ru/pic/office/884-mprev.jpg'
		},
		coords: [55.779135, 37.667168]
	}, {
		properties: {
			title: 'Отделение на Лермонтовском пр.',
			address: 'Лермонтовский пр., д. 10, к. 1',
			metro: 'Лермонтовский проспект',
			tel: '+7 (495) 984-33-04',
			worktime: '10:00 - 19:00 / ежедневно',
			image: 'http://www.ndv.ru/pic/office/5527-mprev.jpg'
		},
		coords: [55.700628, 37.851754]
	}]
};

export default points;
