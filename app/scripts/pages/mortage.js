import scrollbar from 'perfect-scrollbar';
import slick from 'slick-carousel';
import assign from 'object-assign';

let sliderIsActive = false;

const selectSettings = {
	vertical: true,
	draggable: false,
	infinite: false,
	nextArrow: '<span class="next" />',
	prevArrow: '<span class="prev" />'
};

const page = {
	initialize: function() {
		page.updateSelector();
		$(window).on('resize', () => {
			page.updateSelector();
			page.updateScrollbar();
		});

		$('.mortage-table__tab').on('click', function() {
			$(this.parentNode).find('.mortage-table__tab').removeClass('is-active');
			$(this).addClass('is-active');
			const num = $(this).attr('data-num');
			$(this).parents('.mortage-table').find('.mortage-table__table').removeClass('is-active').eq(num).addClass('is-active');
			page.updateScrollbar();
		})
	},

	updateScrollbar: function() {
		scrollbar.update(document.querySelector('.mortage-table .scrollbar'));
	},

	updateSelector: function() {
		const containerHeight = $('.mortage-selector').parent().height();
		const itemHeight = $('.mortage-selector__item').get(0).offsetHeight + 15;
		const itemsLength = $('.mortage-selector__item').size();
		const controlPadding = 100;

		if (Math.floor(containerHeight / itemHeight) < itemsLength) {
			sliderIsActive && $('.mortage-selector').slick('unslick');
			$('.mortage-selector').addClass('is-collapsed');
			$('.mortage-selector').slick(assign({}, selectSettings, {rows: Math.floor(containerHeight / (itemHeight + controlPadding))}));
			sliderIsActive = true;
		} else {
			sliderIsActive && $('.mortage-selector').removeClass('is-collapsed').slick('unslick');
			sliderIsActive = false;
		}
	},

	bindHandlers: function() {

	}
};

export default page;
