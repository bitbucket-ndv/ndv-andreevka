import videojs from 'video.js';

export default function() {
	$('.camera-switcher').on('click', '.camera-switcher__item', function() {
		const $elm = $(this);
		$elm.parents('.camera-viewport').find('iframe').attr('src', $elm.attr('data-src'));
		$elm
			.addClass('active')
			.siblings().removeClass('active');
	});

	$('.interactive-nav__item_cam, .action-camera').fancybox({
		beforeShow: function() {
			const container = $('#camera');
			const sourceUrl = container.find('.camera-switcher__item.active').attr('data-src') || 'about:blank';
			container.find('iframe').attr('src', sourceUrl);
		},
		afterClose: function() {
			$('#camera').find('iframe').attr('src', 'about:blank');
		}
	});
}
