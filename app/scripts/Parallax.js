import page from './utils/page';
import util from 'util';

class Parallax {
	constructor(opts) {
		const defaultOptions = {
			container: document,
			element: '.parallax-element',
			speed: 0.015,
		};

		this.active = false;
		let options = this.options = util._extend(defaultOptions, opts);
		this.container = typeof options.container === 'string' ? document.querySelector(options.container) : options.container;
		this.elements = Array.prototype.slice.call(this.container.querySelectorAll(options.element));
		this.handleMouseOver = this.handleMouseOver.bind(this);
		this.activate();
	}

	activate() {
		this.active = true;
		document.addEventListener('mousemove', this.handleMouseOver, false);
	}

	deactivate() {
		this.active = false;
		document.removeEventListener('mousemove', this.handleMouseOver);
	}

	handleMouseOver(event) {
		const [pageWidth, pageHeight] = page.getPageDimension();
		const [cursorX, cursorY] = [event.clientX, event.clientY];
		this.elements.forEach(element => {
			if (element.className.indexOf('parallax-off') !== -1) {
				return;
			}
			const speed = element.getAttribute('data-speed') || this.options.speed;
			element.style.transform = `translate3d(${(pageWidth / 2 - cursorX) * speed}px, ${(pageHeight / 2 - cursorY) * speed}px, 0)`;
		});
	}
}

export default Parallax;
