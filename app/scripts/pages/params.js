import scrollbar from 'perfect-scrollbar';
import '../libs/rangeSlider';

const sorting = {
	initialize: () => {
		sorting.data = [];

		$('.sort-table tbody tr').each(function() {
			const onclick = $(this).attr('onclick');
			const queryString = onclick.match(/\'(.*?)\'/);
			const href = '/select-apart' + queryString[1];
			const cells = $.makeArray($(this).find('td'));
			sorting.data.push({
				href: href,
				columns: $.map(cells, elm => parseInt($(elm).text().replace(/\s+/g, '').replace(',', '.'), 10))
			});
			$(this).attr('onmouseover', '').attr('onmouseout', '').css('background-color', '').attr('onclick', '').unbind('onclick').addClass('sale');
		});

		sorting.filtered = sorting.data.slice();

		$('.param-table th').on('click', function() {
			const sortType = $(this).hasClass('asc') ? -1 : 1;
			$(this).toggleClass('asc').toggleClass('desc');
			sorting.sortColumnId = $(this).attr('data-sort');
			sorting.sortByNum(sortType);
		});
	},

	sortByNum: function(sortType) {
		sorting.filtered.sort((a, b) => sortType * (a.columns[sorting.sortColumnId] - b.columns[sorting.sortColumnId]));
		sorting.render(sorting.data);
	},

	filter: function(columnId, from, to) {
		sorting.filtered = sorting.data.filter(item => item[columnId] >= from && item[columnId] <= to);
		sorting.render();
	},

	render: function() {
		$('.sort-table tbody').remove();
		const $tbody = $('<tbody />');
		sorting.filtered.forEach(item => {
			const $tr = $('<tr class="sale" />').data('href', item.href);
			item.columns.forEach((cell, cellNum) => {
				let text = cell;
				if (cellNum === 7 || cellNum === 6) {
					text = cell.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
				}
				$tr.append($(`<td>${text}</td>`))
			});
			$tbody.append($tr);
		});
		$('.sort-table').append($tbody);
		scrollbar.update(document.querySelector('.scrollbar'));
	}
}

export default {
	initialize: function() {
		sorting.initialize();

		$('#range1').ionRangeSlider({
			type: 'double',
			min: 1,
			max: 17,
			hide_min_max: true,
			hide_from_to: false,
			grid: false,
			values_separator: ';',
			onChange: function(data) {
				sorting.filter(1, data.from, data.to);
			}
		});

		$('#range2').ionRangeSlider({
			type: 'double',
			min: 1,
			max: 3,
			hide_min_max: true,
			hide_from_to: false,
			grid: false,
			values_separator: ';',
			onChange: function(data) {
				sorting.filter(4, data.from, data.to);
			}
		});

		$('#range3').ionRangeSlider({
			type: 'double',
			min: 1,
			max: 5,
			hide_min_max: true,
			hide_from_to: false,
			grid: false,
			values_separator: ';',
			onChange: function(data) {
				sorting.filter(7, data.from, data.to);
			}
		});

		$('.genplan-button').fancybox({href: $('.genplan-button').attr('data-href')});
	}
};
