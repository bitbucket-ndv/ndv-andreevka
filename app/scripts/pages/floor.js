
const page = {
	initialize: function() {
		page.bindHandlers();
	},

	bindHandlers: function() {
		$('.select-box .select-box__prev').on('click', function() {
			const $parent = $(this).parents('.select-box');
			let num = Math.max(parseInt($parent.find('.select-box__num').text(), 10) - 1, 0);
			$parent.find('.select-box__num').text(num);
		});
		$('.select-box .select-box__prev').on('click', function() {
			const $parent = $(this).parents('.select-box');
			let num = Math.min(parseInt($parent.find('.select-box__num').text(), 10) + 1, 20);
			$parent.find('.select-box__num').text(num);
		})
	}
};

export default page;
