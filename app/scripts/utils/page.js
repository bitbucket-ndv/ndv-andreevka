
let page = {
	width: document.body.offsetWidth,
	height: document.body.offsetHeight,
};

window.addEventListener('resize', () => {
	[page.width, page.width] = [document.body.offsetWidth, document.body.offsetHeight];
});

export default {
	getPageDimension: function() {
		return [page.width, page.width];
	}
}